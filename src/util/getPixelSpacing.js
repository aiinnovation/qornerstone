import _ from 'lodash';
import dicomParser from 'dicom-parser';
import external from '../externalModules';
import getNumberValues from './getNumberValues';

export default function getPixelSpacing(image) {
  const imagePlane = external.cornerstone.metaData.get(
    'imagePlaneModule',
    image.imageId
  );

  const dataSet = dicomParser.parseDicom(image.data.byteArray);
  const imagerPixelSpacing = getNumberValues(dataSet, 'x00181164', 2);

  return {
    rowPixelSpacing:
      _.get(imagePlane, ['rowPixelSpacing'], 0) ||
      _.get(imagerPixelSpacing, ['0'], 0) ||
      _.get(image, ['rowPixelSpacing']),
    colPixelSpacing:
      _.get(imagePlane, ['columnPixelSpacing'], 0) ||
      _.get(imagerPixelSpacing, ['1'], 0) ||
      _.get(image, ['columnPixelSpacing']),
  };
}
