import external from '../externalModules.js';
import BaseTool from './base/BaseTool.js';
import _ from 'lodash';
import { wwwcCursor } from './cursors/index.js';

/**
 * @public
 * @class WwwcTool
 * @memberof Tools
 *
 * @classdesc Tool for setting wwwc by dragging with mouse/touch.
 * @extends Tools.Base.BaseTool
 */
export default class TouchMoveTool extends BaseTool {
  constructor(props = {}) {
    const defaultProps = {
      name: 'TouchMove',
      strategies: { scrollStrategy },
      supportedInteractionTypes: ['Touch'],
      // configuration: {
      //   onScroll: () => {},
      // },
      svgCursor: '',
    };

    super(props, defaultProps);
  }

  touchDragCallback(evt) {
    // Prevent CornerstoneToolsTouchStartActive from killing any press events
    evt.stopImmediatePropagation();
    this.applyActiveStrategy(evt);
    // External.cornerstone.setViewport(evt.detail.element, evt.detail.viewport);
  }
}

function scrollStrategy(evt) {
  /* eslint-disable */

  const lastDeltaY = parseInt(
    _.get(evt, ['detail', 'lastPoints', 'page', 'y'], 0)
  );
  const { onScroll } = this.configuration;

  const deltaY = parseInt(
    _.get(evt, ['detail', 'deltaPoints', 'page', 'y'], 0)
  );

  if (deltaY < 0) {
    onScroll(0);
  }
  if (deltaY > 0) {
    onScroll(1);
  }

  // If (deltaY === 0) {
  //   return false;
  // }
  // Try {
  //   if (deltaY > 0) {
  //     onScroll(0);
  //   }
  //   if (deltaY < 1) {
  //     onScroll(1);
  //   }

  //   lastDeltaY = deltaY;
  // } catch (err) {
  //   console.log(err);
  // }
}
