import external from './../../externalModules.js';
import BaseAnnotationTool from '../base/BaseAnnotationTool.js';
// State
import {
  addToolState,
  getToolState,
  removeToolState,
} from './../../stateManagement/toolState.js';
import toolStyle from './../../stateManagement/toolStyle.js';
import textStyle from './../../stateManagement/textStyle.js';
import toolColors from './../../stateManagement/toolColors.js';
// Manipulators
import { moveNewHandle } from './../../manipulators/index.js';
// Drawing
import {
  getNewContext,
  draw,
  setShadow,
  drawLine,
} from './../../drawing/index.js';
import drawLinkedTextBox from './../../drawing/drawLinkedTextBox.js';
import drawHandles from './../../drawing/drawHandles.js';
import lineSegDistance from './../../util/lineSegDistance.js';
import roundToDecimal from './../../util/roundToDecimal.js';
import { cobbAngleCursor } from '../cursors/index.js';
import triggerEvent from '../../util/triggerEvent.js';
import EVENTS from '../../events.js';
import getPixelSpacing from '../../util/getPixelSpacing';
import throttle from '../../util/throttle';
import { getModule } from '../../store/index';

/**
 * @public
 * @class CTRTool
 * @memberof Tools.Annotation
 * @classdesc Create and position an angle by placing three consecutive points.
 * @extends Tools.Base.BaseAnnotationTool
 * @hideconstructor
 *
 * @param {ToolConfiguration} [props={}]
 */
export default class CTRTool extends BaseAnnotationTool {
  constructor(props = {}) {
    const defaultProps = {
      name: 'CTR',
      supportedInteractionTypes: ['Mouse', 'Touch'],
      svgCursor: cobbAngleCursor,
      configuration: {
        drawHandles: true,
        drawHandlesOnHover: false,
        hideHandlesIfMoving: false,
        renderDashed: false,
        hasCTRAngleInPercentage: false,
      },
    };

    super(props, defaultProps);

    this.hasIncomplete = false;

    this.throttledUpdateCachedStats = throttle(this.updateCachedStats, 110);
  }

  createNewMeasurement(eventData) {
    // Create the measurement data for this tool with the end handle activated
    this.hasIncomplete = true;

    return {
      visible: true,
      active: true,
      color: undefined,
      invalidated: true,
      complete: false,
      handles: {
        start: {
          x: eventData.currentPoints.image.x,
          y: eventData.currentPoints.image.y,
          highlight: true,
          active: false,
        },
        end: {
          x: eventData.currentPoints.image.x,
          y: eventData.currentPoints.image.y,
          highlight: true,
          active: true,
        },
        start2: {
          x: eventData.currentPoints.image.x,
          y: eventData.currentPoints.image.y,
          highlight: true,
          active: false,
          drawnIndependently: true,
        },
        end2: {
          x: eventData.currentPoints.image.x,
          y: eventData.currentPoints.image.y,
          highlight: true,
          active: false,
          drawnIndependently: true,
        },
        textBox: {
          active: false,
          hasMoved: false,
          movesIndependently: false,
          drawnIndependently: true,
          allowedOutsideImage: true,
          hasBoundingBox: true,
        },
        textBox2: {
          active: false,
          hasMoved: false,
          movesIndependently: false,
          drawnIndependently: true,
          allowedOutsideImage: true,
          hasBoundingBox: true,
        },
        textBoxCTR: {
          active: false,
          hasMoved: false,
          movesIndependently: false,
          drawnIndependently: true,
          allowedOutsideImage: true,
          hasBoundingBox: true,
        },
        textBoxLen: {
          active: false,
          hasMoved: false,
          movesIndependently: false,
          drawnIndependently: true,
          allowedOutsideImage: true,
          hasBoundingBox: true,
        },
        textBoxLen1: {
          active: false,
          hasMoved: false,
          movesIndependently: false,
          drawnIndependently: true,
          allowedOutsideImage: true,
          hasBoundingBox: true,
        },
      },
    };
  }

  /**
   *
   *
   * @param {*} element
   * @param {*} data
   * @param {*} coords
   * @returns {Boolean}
   */
  pointNearTool(element, data, coords) {
    if (data.visible === false) {
      return false;
    }

    if (this.hasIncomplete) {
      return false;
    }

    return (
      lineSegDistance(element, data.handles.start, data.handles.end, coords) <
        25 ||
      lineSegDistance(element, data.handles.start2, data.handles.end2, coords) <
        25
    );
  }

  updateCachedStats(image, element, data) {
    const { hasCTRAngleInPercentage } = this.configuration;
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    const dx1 =
      (data.handles.end.x - data.handles.start.x) * (colPixelSpacing || 1);
    const dy1 =
      (data.handles.end.y - data.handles.start.y) * (rowPixelSpacing || 1);
    const dx2 =
      (data.handles.end2.x - data.handles.start2.x) * (colPixelSpacing || 1);
    const dy2 =
      (data.handles.end2.y - data.handles.start2.y) * (rowPixelSpacing || 1);

    const length1 = Math.sqrt(dx1 * dx1 + dy1 * dy1);
    const length2 = Math.sqrt(dx2 * dx2 + dy2 * dy2);

    data.length1 = roundToDecimal(length1, 2);
    data.length2 = roundToDecimal(length2, 2);
    let ctrlength = length1 / length2;
    if (hasCTRAngleInPercentage) {
      ctrlength = ctrlength * 100;
      data.rAngle = +roundToDecimal(ctrlength, 2);
    } else {
      data.rAngle = +roundToDecimal(ctrlength, 2).toFixed(2);
    }
    data.invalidated = false;
  }

  renderToolData(evt) {
    const eventData = evt.detail;
    const {
      handleRadius,
      drawHandlesOnHover,
      hideHandlesIfMoving,
      renderDashed,
    } = this.configuration;
    // If we have no toolData for this element, return immediately as there is nothing to do
    const toolData = getToolState(evt.currentTarget, this.name);

    if (!toolData) {
      return;
    }

    // We have tool data for this element - iterate over each one and draw it
    const context = getNewContext(eventData.canvasContext.canvas);

    const lineWidth = toolStyle.getToolWidth();
    const lineDash = getModule('globalConfiguration').configuration.lineDash;
    const font = textStyle.getFont();

    for (let i = 0; i < toolData.data.length; i++) {
      const data = toolData.data[i];

      if (data.visible === false) {
        continue;
      }

      draw(context, context => {
        setShadow(context, this.configuration);

        // Differentiate the color of activation tool
        const color = toolColors.getColorIfActive(data);

        const lineOptions = { color };

        if (renderDashed) {
          lineOptions.lineDash = lineDash;
        }

        drawLine(
          context,
          eventData.element,
          data.handles.start,
          data.handles.end,
          lineOptions
        );

        if (data.complete) {
          drawLine(
            context,
            eventData.element,
            data.handles.start2,
            data.handles.end2,
            lineOptions
          );
        }

        // Draw the handles
        const handleOptions = {
          color,
          handleRadius,
          drawHandlesIfActive: drawHandlesOnHover,
          hideHandlesIfMoving,
        };

        if (this.configuration.drawHandles) {
          drawHandles(context, eventData, data.handles, handleOptions);
        }

        // Draw the text
        context.fillStyle = color;

        if (!data.handles.textBox.hasMoved) {
          const textCoords = {
            x: (data.handles.start.x + data.handles.end.x) / 2,
            y: (data.handles.start.y + data.handles.end.y) / 2 - 10,
          };

          context.font = font;
          data.handles.textBox.x = textCoords.x;
          data.handles.textBox.y = textCoords.y;

          // For the showing pixel mm
          const coords = {
            x: Math.max(data.handles.start.x, data.handles.end.x),
          };

          // Depending on which handle has the largest x-value,
          // Set the y-value for the text box
          if (coords.x === data.handles.start.x) {
            coords.y = data.handles.start.y;
          } else {
            coords.y = data.handles.end.y;
          }

          data.handles.textBoxLen.x = coords.x + 20;
          data.handles.textBoxLen.y = coords.y;
        }

        if (!data.handles.textBox2.hasMoved) {
          const textCoords = {
            x: (data.handles.start2.x + data.handles.end2.x) / 2,
            y: (data.handles.start2.y + data.handles.end2.y) / 2 - 5,
          };

          context.font = font;
          data.handles.textBox2.x = textCoords.x;
          data.handles.textBox2.y = textCoords.y;

          // For the showing pixel mm
          const coords = {
            x: Math.max(data.handles.start2.x, data.handles.end2.x),
          };

          // Depending on which handle has the largest x-value,
          // Set the y-value for the text box
          if (coords.x === data.handles.start2.x) {
            coords.y = data.handles.start2.y;
          } else {
            coords.y = data.handles.end2.y;
          }

          data.handles.textBoxLen1.x = coords.x + 20;
          data.handles.textBoxLen1.y = coords.y;
        }

        if (!data.handles.textBoxCTR.hasMoved) {
          const textCoords = {
            x: (data.handles.start2.x + data.handles.end2.x) / 2,
            y: data.handles.start2.y + 90,
          };

          context.font = font;
          data.handles.textBoxCTR.x = textCoords.x;
          data.handles.textBoxCTR.y = textCoords.y;
        }

        drawLinkedTextBox(
          context,
          eventData.element,
          data.handles.textBox,
          'Cardiac',
          data.handles,
          textBoxAnchorPoints,
          color,
          lineWidth,
          0,
          true
        );

        drawLinkedTextBox(
          context,
          eventData.element,
          data.handles.textBoxLen,
          data.value,
          data.handles,
          textBox2AnchorPoints,
          color,
          lineWidth,
          0,
          true
        );

        const textCTR = data.ctrvalue;

        if (data.complete) {
          drawLinkedTextBox(
            context,
            eventData.element,
            data.handles.textBox2,
            'Thoracic',
            data.handles,
            textBox2AnchorPoints,
            color,
            lineWidth,
            0,
            true
          );

          drawLinkedTextBox(
            context,
            eventData.element,
            data.handles.textBoxCTR,
            textCTR,
            data.handles,
            textBox2AnchorPoints,
            color,
            lineWidth,
            0,
            true
          );

          drawLinkedTextBox(
            context,
            eventData.element,
            data.handles.textBoxLen1,
            data.value2,
            data.handles,
            textBox2AnchorPoints,
            color,
            lineWidth,
            0,
            true
          );
        }
      });
    }

    function textBoxAnchorPoints(handles) {
      const midpoint = {
        x: (handles.start.x + handles.end.x) / 2,
        y: (handles.start.y + handles.end.y) / 2 + 10,
      };

      return [handles.start, midpoint, handles.end];
    }

    function textBox2AnchorPoints(handles) {
      const midpoint = {
        x: (handles.start2.x + handles.end2.x) / 2,
        y: (handles.start2.y + handles.end2.y) / 2,
      };

      return [handles.start2, midpoint, handles.end2];
    }
  }
  getIncomplete(element) {
    const toolState = getToolState(element, this.name);

    if (toolState && Array.isArray(toolState.data)) {
      return toolState.data.find(({ complete }) => complete === false);
    }
  }

  addNewMeasurement(evt, interactionType) {
    evt.preventDefault();
    evt.stopPropagation();

    const eventData = evt.detail;

    let measurementData;
    let toMoveHandle;
    let doneMovingCallback = success => {
      // DoneMovingCallback for first measurement.
      if (!success) {
        removeToolState(element, this.name, measurementData);

        return;
      }
    };

    // Search for incomplete measurements
    const element = evt.detail.element;
    const pendingMeasurement = this.getIncomplete(element);

    if (pendingMeasurement) {
      measurementData = pendingMeasurement;
      measurementData.complete = true;
      measurementData.handles.start2 = {
        x: eventData.currentPoints.image.x,
        y: eventData.currentPoints.image.y,
        drawnIndependently: false,
        highlight: true,
        active: false,
      };
      measurementData.handles.end2 = {
        x: eventData.currentPoints.image.x,
        y: eventData.currentPoints.image.y,
        drawnIndependently: false,
        highlight: true,
        active: true,
      };
      toMoveHandle = measurementData.handles.end2;
      this.hasIncomplete = false;
      doneMovingCallback = success => {
        // DoneMovingCallback for second measurement
        if (!success) {
          removeToolState(element, this.name, measurementData);

          return;
        }

        const eventType = EVENTS.MEASUREMENT_COMPLETED;
        const eventData = {
          toolName: this.name,
          toolType: this.name, // Deprecation notice: toolType will be replaced by toolName
          element,
          measurementData,
        };

        triggerEvent(element, eventType, eventData);
      };
    } else {
      measurementData = this.createNewMeasurement(eventData);
      addToolState(element, this.name, measurementData);
      toMoveHandle = measurementData.handles.end;
    }

    // Associate this data with this imageId so we can render it and manipulate it
    external.cornerstone.updateImage(element);

    moveNewHandle(
      eventData,
      this.name,
      measurementData,
      toMoveHandle,
      this.options,
      interactionType,
      doneMovingCallback
    );
  }

  onMeasureModified(ev) {
    const { element } = ev.detail;
    const { hasCTRAngleInPercentage } = this.configuration;
    const image = external.cornerstone.getEnabledElement(element).image;
    const { rowPixelSpacing, colPixelSpacing } = getPixelSpacing(image);

    if (ev.detail.toolName !== this.name) {
      return;
    }
    const data = ev.detail.measurementData;

    // Update textbox stats
    if (data.invalidated === true) {
      if (data.length1) {
        this.throttledUpdateCachedStats(image, element, data);
      } else {
        this.updateCachedStats(image, element, data);
      }
    }

    const { rAngle, length1, length2 } = data;

    data.value = '';
    data.value2 = '';
    data.ctrvalue = '';

    if (!Number.isNaN(length1) && Number.isFinite(length1)) {
      data.value = textBoxText(length1, rowPixelSpacing, colPixelSpacing);
    }
    if (!Number.isNaN(length2) && Number.isFinite(length2)) {
      data.value2 = textBoxText(length2, rowPixelSpacing, colPixelSpacing);
    }
    if (!Number.isNaN(rAngle) && Number.isFinite(rAngle)) {
      const unit = hasCTRAngleInPercentage ? '%' : '';
      data.ctrvalue = `CTR: ${rAngle.toString()}${unit}`;
    }
    function textBoxText(length, rowPixelSpacing, colPixelSpacing) {
      // Set the length text suffix depending on whether or not pixelSpacing is available
      let suffix = ' mm';

      if (!rowPixelSpacing || !colPixelSpacing) {
        suffix = ' pixels';
      }

      return length.toString() + suffix;
    }
  }

  activeCallback(element) {
    this.onMeasureModified = this.onMeasureModified.bind(this);
    element.addEventListener(
      EVENTS.MEASUREMENT_MODIFIED,
      this.onMeasureModified
    );
  }

  passiveCallback(element) {
    this.onMeasureModified = this.onMeasureModified.bind(this);
    element.addEventListener(
      EVENTS.MEASUREMENT_MODIFIED,
      this.onMeasureModified
    );
  }

  enabledCallback(element) {
    element.removeEventListener(
      EVENTS.MEASUREMENT_MODIFIED,
      this.onMeasureModified
    );
  }

  disabledCallback(element) {
    element.removeEventListener(
      EVENTS.MEASUREMENT_MODIFIED,
      this.onMeasureModified
    );
  }
}
