# Qure Cornerstone

## Getting started 

```
// installs the package 
npm i

// start the dev server
npm start
  
```

## Publishing the package 

1- Do the npm prod build

`export NODE_ENV=production; npm run build:prod`


2- Bump the version in the package.json, either minor or pre-release

3- Make sure you have logged into the Qure npm with given credentials

4- Publish the package to the Qure npm

`npm publish` 


